<?php
    include "show-data.php";
    include "update-data.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="asset/css/style.css">
    <title>Profil Saya</title>
    <meta name="viewport" content="width=device-width,
    initial-scale=1.0">
</head>
<body>
    <nav>
        <div class="menu-mobile">
            <a href="#" onclick="showmenu()">Menu</a>
        </div>
        <ul id="menu">
            <li><a href="#">HOME</a></li>
            <li><a href="#">PRODUCT</a></li>
            <li><a href="#">GALLERY</a></li>
            <li><a href="#">NEWS</a></li>
            <li><a href="#">MY INVENTORY</a></li>
        </ul>
    </nav>
    <section id="box-profile">
        <div class="img-profile">
            <div class="photo" style="background-image:url(asset/img/1.jpg);">
            </div>
        </div>
        <div class="description">
            <h1 id="pName"><?php echo $nama ?></h1>
            <p id="pRole"><?php echo $role ?></p>
            <a href="#input-form"class="button bg-green" onclick="editForm()">Edit</a>
            <a href="#"class="button border-blue">Resume</a>
        </div>
        <div class="information">
            <div class="data">
                <p class="field">Availability</p>
                <p class="text-gray" id="pAvailability"><?php echo $availability ?></p>
            </div>
            <div class="data">
                <p class="field">Age</p>
                <p class="text-gray" id="pAge"><?php echo $age ?></p>
            </div>
            <div class="data">
                <p class="field">Location</p>
                <p class="text-gray" id="pLocation"><?php echo $location ?></p>
            </div>
            <div class="data">
                <p class="field">Years Experience</p>
                <p class="text-gray" id="pYears"><?php echo $years ?></p>
            </div>
            <div class="data">
                <p class="field">Email</p>
                <p class="text-gray" id="pEmail"><?php echo $email ?></p>
            </div>
        </div>
    </section>
    <section id="input-form">
    <form method="POST" action="<?php echo$_SERVER['PHP_SELF']; ?>">
            <div class="form">
            <label>ID</label>
            <input type="text" id="inpId" name="id" value="<?php echo $id; ?>">
        </div>
        <div class="form">
            <label>Name</label>
            <input type="text" id="inpName" name="nama">
        </div>
        <div class="form">
            <label>Role</label>
            <input type="text" id="inpRole" name="role">
        </div>
        <div class="form">
            <label>Availability</label>
            <input type="text" id="inpAvailability" name="availability">
        </div>
        <div class="form">
            <label>Age</label>
            <input type="number" id="inpAge" name="age">
        </div>
        <div class="form">
            <label>Location</label>
            <input type="text" id="inpLocation" name="location">
        </div>
        <div class="form">
            <label>Years Experience</label>
            <input type="number" id="inpYears" name="years">
        </div>
        <div class="form">
            <label>Email</label>
            <input type="email" id="inpEmail" name="email">
        </div>
        <div class="form">
            <input type="submit" onclick="" name="submit" value="SUBMIT" class="bg-blue">
        </div>
        </form>
    </section>
    <script>
        var formMenu = document.getElementById("input-form");
        formMenu.style.display = "none"
       

        function editForm(){
            if(formMenu.style.display === "none"){
                formMenu.style.display = "block";
            }else{
                formMenu.style.display = "none";
            }
            var name = document.getElementById("pName").innerHTML;
            var role = document.getElementById("pRole").innerHTML;
            var availability = document.getElementById("pAvailability").innerHTML;
            var age = document.getElementById("pAge").innerHTML;
            var location = document.getElementById("pLocation").innerHTML;
            var years = document.getElementById("pYears").innerHTML;
            var email = document.getElementById("pEmail").innerHTML;

            
            
            document.getElementById("inpName").value =name;
            document.getElementById("inpRole").value =role;
            document.getElementById("inpAvailability").value =availability;
            document.getElementById("inpAge").value =age;
            document.getElementById("inpLocation").value =location;
            document.getElementById("inpYears").value =years;
            document.getElementById("inpEmail").value =email;

            
        }
            function showmenu(){
            var menu = document.getElementById("menu");
            var box = document.getElementById("box-profile");
            
            if(menu.style.display === "block"){
                menu.style.display = "none";
                box.style.paddingTop = "0px";
            }else{
                menu.style.display = "block";
                box.style.paddingTop= "125px";
            }
            }
    </script>
</body>
</html>